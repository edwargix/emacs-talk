\documentclass{beamer}
\usepackage{nth}
\usepackage{minted}


\title{Emacs}
\author{David Florness}
\date{October \nth{3}, 2019}


\usetheme{Madrid}
\usecolortheme{whale}


\begin{document}
\begin{frame}
  \titlepage

  \begin{center}
    \includegraphics[width=0.35\textwidth]{manual.jpg}
  \end{center}
\end{frame}

\begin{frame}{We spend a lot of time editing text}
  \pause
  \begin{enumerate}
  \item writing programs \pause
  \item writing essays \pause
  \item taking notes \pause
  \item writing email \pause
  \end{enumerate}
  \vspace{0.5cm}

  Why not create a program to make typing text as easy as possible? \pause \\

  Emacs is one such attempt at a program. \pause \\

  Let me show you why it's the best attempt thus far.
\end{frame}

\begin{frame}[fragile]{What is Emacs?}
  \pause

  A ``display editor'' designed and written in the 1970's by Richard Stallman,
  who was very impressed with the Stanford AI Lab's \textit{E} editor and wanted
  to add \textit{macros} to it. The name ``Emacs'' means Editing Macros. \pause \\

  Officially, Emacs is ``the extensible, customizable, self-documenting,
  real-time display editor.'' (in shell: \texttt{info emacs})
\end{frame}

\begin{frame}{Display Editor?}
  \pause
  From Richard Stallman's 1981 paper, ``EMACS:\ The Extensible, Customizable
  Display Editor'' that was delivered during the ACM conference on Text
  Processing: \\

  \begin{quote}
    By a display editor we mean an editor in which the text being edited is
    normally visible on the screen and is updated automatically as the user
    types his commands. No explicit commands to `print' text are needed.
  \end{quote}
\end{frame}

\begin{frame}{Customizable?}
  \pause
  \begin{enumerate}
  \item Everything in Emacs is either a \pause
    \begin{itemize}
    \item LISP variable waiting to be changed (\texttt{C-h v})

      everything you see on the screen can be modified \pause
      
    \item LISP function waiting to be invoked (\texttt{C-h c})

      all keys you type simply call LISP functions that perform operations on
      the text \pause
    \end{itemize}
  \item you may think of Emacs as a LISP machine trapped inside of a program
  \end{enumerate}
\end{frame}

\begin{frame}{Extensible?}
  \pause
  \begin{enumerate}
  \item While Emacs is running, you can tell Emacs to load new bits of LISP code
    for your use \pause
  \item While the core of Emacs is written in C, most of it is bits of LISP code
    that define how the editor behaves \pause
  \item Emacs can install packages
  \item Packages are nothing more than tarballs of source-code files containing
    LISP function \& variable declarations
  \end{enumerate}
\end{frame}

\begin{frame}{Interacting with Emacs}
  \pause

  Most keys simply insert themself into the buffer \pause \\

  Others (usually starting cith CTRL or ALT) call LISP functions \pause \\

  If you want to call a LISP function that has no associated key, do
  \texttt{M-x} (i.e. ALT-x)
\end{frame}

\begin{frame}[fragile]{A word about keybindings}
  \pause Vim's keybindings are better. \pause Anyone who says otherwise is
  lying. \pause \\

  So install Evil Mode, the \textbf{E}xtensible \textbf{VI} \textbf{L}ayer for
  Emacs:

  \begin{minted}{elisp}
    (package-install 'evil)
    (evil-mode)
  \end{minted}
  \pause

  Having said that, the Emacs bindings are worth knowing. They're the default in
  most shells and in GNU readline, which is used by just about every REPL. \pause \\

  The bindings themself were created for the space cadet keyboard
\end{frame}

\begin{frame}{The legendary Emacs packages}
  Let's take a look at some of the greatest Emacs packages. \pause
  \begin{enumerate}
  \item Dired (builtin) --- easily traverse filesystem and perform file
    operations \pause
  \item Ivy/Helm --- turn \texttt{M-x} into a fuzzy finder \pause
  \item Magit --- the greatest interface to Git \pause
  \item AUCTeX --- features to make typing {La,}TeX easier \pause
  \item SLIME/CIDER/Geiser --- interactive programming for LISPs \pause
  \item Yasnippet --- snippets \pause
  \item Language server protocol client via \texttt{lsp-mode} or \texttt{eglot}
    \begin{itemize}
    \item gives you IDE-like editing features for any programming language that
      has an LSP server (most do) \pause
    \end{itemize}
  \item mu4e / gnus --- read email inside Emacs \pause
  \item comint mode (builtin) --- your buffer is a REPL; makes interactive
    programming a breeze
  \end{enumerate}
\end{frame}

\begin{frame}{Emacs vs UNIX}
  \begin{figure}
    \begin{minipage}{0.4\textwidth}
      \includegraphics[width=0.9\textwidth]{bram.jpg}
    \end{minipage}
    \begin{minipage}{0.4\textwidth}
      \begin{flushleft}
        Whoa there! \pause Emacs is a blatant violator of the UNIX philosophy:
        every program should do one thing, well! \pause
        \vspace{0.5cm}

        Every Vim user ever: \pause
        \begin{quote}
          Emacs is a great operating system, lacking only a decent editor.
        \end{quote}
      \end{flushleft}
    \end{minipage}
  \end{figure}
\end{frame}

\begin{frame}{Emacs loves UNIX}
  Emacs does not try to replace those beloved UNIX commands, but rather gives a
  clean, consistent, interactive way to use them. \pause \\

  \texttt{M-x grep} \pause \\

  In the Magit status buffer, type \texttt{\$} to see that Magit is useing Git
  directly \pause \\

  Dired is just an interactive \texttt{cd} and \texttt{ls}.
\end{frame}

\begin{frame}[fragile]{On the subject of startup times}
  \begin{enumerate}
  \item Vim takes \textit{way} less time to startup \pause
  \item But so what? \pause Why in the world are you rebooting your editor every
    time you want to edit a file? \pause
  \item Emacs can be run in daemon mode:

\begin{verbatim}
M-x server-start
\end{verbatim}

  \item Then, you can connect to the Emacs server with the Emacs client:
\begin{verbatim}
$ emacsclient file
\end{verbatim}
  \end{enumerate}
\end{frame}

\begin{frame}[allowframebreaks]{So you want to give it a shot\dots}
  There is a builtin tutorial

  \begin{center}
    \includegraphics[width=0.60\textwidth]{tutorial-0.png}
  \end{center}

  \framebreak

  \begin{center}
    \includegraphics[width=0.60\textwidth]{tutorial-1.png}
  \end{center}

  \framebreak

  Spacemacs

  \begin{center}
    \includegraphics[width=0.6\textwidth]{spacemacs-0.png}
  \end{center}

  \framebreak

  \begin{center}
    \includegraphics[width=0.6\textwidth]{spacemacs-1.png}
  \end{center}
\end{frame}

\end{document}